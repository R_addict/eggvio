# EggVio

A metagenomic pipeline for fast automated assembly and binning using megahit and anvio. As well as meta genome annotation using eggnog mapper on both contigs and single reads not mapped on contigs.