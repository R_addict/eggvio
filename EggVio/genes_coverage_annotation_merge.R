#!/usr/bin/env Rscript
#author:Benoit Bergk Pinto
#aim: Merge annotations with the genes coverage in read counts to get an abundance table

#for debug;
#args=c("final.contigs_anvio_2500.emapper.annotations","geneNames.txt","read_counts_final.tab" )

library(data.table)
args = commandArgs(trailingOnly=TRUE)

if (length(args)<2) {
  stop("The script requires annotation file from emapper, a file with  coverage computed on genes predicted by prodigal as arguments", call.=FALSE)
} else if (length(args)==2) {
  # default output file
  print("Merging annotations and coverage")
}

eggAnnot <- fread(file=args[1], sep='\t',header=TRUE)

colnames(eggAnnot)[1] <- gsub(pattern='#',replacement='',colnames(eggAnnot)[1])

covSummary <- read.table(file=args[2],row.names=1, sep='\t',header=TRUE,stringsAsFactors=FALSE)

#Now contigs reads counts is normalized by their amount of genes so total count for annotation will stay constant or lower if some of the genes submitted aren't annotated.
print('Initialize result matrix for genes')
finalRes <- data.frame(matrix(NA,nrow=length(unique(eggAnnot$seed_eggNOG_ortholog)),ncol =ncol(covSummary)))
#add colnames = sample names
colnames(finalRes) <- colnames(covSummary)
#rownames = unique annotations from eggnog
rownames(finalRes) <- unique(eggAnnot$seed_eggNOG_ortholog)
print('Computing read counts for all genes detected and merging their annotations with their respective abundances')
#table to store results is now initialized so let's start filling it
for(rowName in rownames(finalRes)){
	contigsHits <- which( eggAnnot$seed_eggNOG_ortholog == rowName)
	genesId <- eggAnnot$query_name[contigsHits]
	finalRes[rowName,] <- colSums(covSummary[genesId,]) #just sum the different genes that have the same annotation
}

#so that's it ;) Let's write it now.

write.table(finalRes,file='final_eggnog_genes_abundance_table.tab', row.names=TRUE,quote =FALSE,sep='\t')
print('Coverage and annotations for contigs have been successfully merged together and saved as final_eggnog_contigs_abundance_table.tab')
#close script
q('no')
