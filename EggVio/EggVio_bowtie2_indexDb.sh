#!/bin/bash
#author: Benoit Bergk pinto
#Date: 19-11-2018
#--------------------------------------
#Objective: build index of contigs to map with bowtie
#variables


#Variables used
#$1 = path to contig FASTA
#$2 = NAME to give to the different elements of the index built
#$3 = number of threads

fastaOfContigs= nameOfIndex='Index' use='normal' lengthThreshold=2500  thread=1

usage="\nUsage: $0 [-f fileOfContigs] [-n nameOfIndex] [-l lengthThreshold] [-t thread] \n
-f\tpath to the fasta of the contigs generated with the assembler. Mandatory parameter.                      
-n\tname to give to the index generated for mapping. (Default:Index)
-l\tonly if -u is set to anvio. The threshold length (in bp) to keep contigs in the db for mapping. (Default: 2500)
-t\tthe number of threads to use with bowtie2 to compute the index. (Default:1) \n"

while getopts :f:n:l:t:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	f) fastaOfContigs=$OPTARG
	;;
	n) nameOfIndex=$OPTARG
	;;
	l) lengthThreshold=$OPTARG
	;;
	t) thread=$OPTARG
     	;;
	h) printf "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1
	     ;;
	esac
done

shift $((OPTIND - 1)) #remove options leave arguments
pathSave=${fastaOfContigs%/*}
nameFasta=$(basename $fastaOfContigs)   # basiclly the name of the fasta contig without the path

##load anvio env                                                                                                                     
source activate anvio_env
#filter out too short contigs and simplify names for anvio
#anvi-script-reformat-fasta $fastaOfContigs -o "${pathSave}/${nameFasta%.fa}_anvio_${lengthThreshold}.fa" -l $lengthThreshold --simplify-names
conda deactivate

source activate mapping_env
#build index db
bowtie2-build --threads $thread -f "$fastaOfContigs" "${pathSave}/$nameOfIndex"

conda deactivate
