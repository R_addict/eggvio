#!/bin/bash
#goal: pipeline using all the EGgVio scripts together in one full big pipeline

#SBATCH --job-name=EggVio_MISS
#SBATCH --output=job.%j.out
#SBATCH --error=job.%j.err
#SBATCH --mail-user=benoit.bergk-pinto@ec-lyon.fr
#SBATCH --mail-type=ALL
#
#SBATCH --partition=mononode
#SBATCH --nodes=1
#SBATCH --cpus-per-task=16
#SBATCH --mem=64000 # amount of RAM memory required per node, in Mega Bytes
#SBATCH --time=15:00:00

rawData=

datasetName=Dataset #name of the dataset to add to anvio's profile

threads=1 # number of threads for scripts which need this argument for running in parallel

kaijuDb= #the path to kaiju db folder to use to get taxonomy for the genes o n the contigs

userName=

partitionName=

durationTime=24

limitNumJobs=10

usage="\nUsage: $0 [-r  rawData] [-n datasetName] [-k kaijuDb] [-u userName] [-p partition] [-d  duration] [-l limitNumJobs]  [-t threads] \n
-r\tpath to the fastq of the reads from the different samples of the dataset before quality filtering. Mandatory parameter. 
-n\ta name for the dataset which is mined. This will be used to generate anvio's profiles and also other directory names for results files. (Default: Dataset)
-k\tthe path to the database to use for kaiju taxonomy annotation.
-u\tthe name of your username session on the server  in order to monitor the amount of jobs submitted in the queue. Mandatory parameter
-p\tthe name of the partition where the jobs should be submitted. Mandatory parameter.
-d\tthe amount of time to run the script (in hours). (Default: 24)
-l\tthe maximum number of jobs to submitt during the read annotation phase of the pipeline. (Default: 10)
-t\tthe number of threads to ask for each of the jobs being submitted. (Default: 1)\n"

while getopts :r:n:l:u:p:d:t:k:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	r) rawData=$OPTARG
	;;
	n) nameOfDataset=$OPTARG
	;;
	l) limitNumJobs=$OPTARG
	;;
	u) userName=$OPTARG
	;;
	p) partitionName=$OPTARG
	;;
	d) duration=$OPTARG
	;;
	t) threads=$OPTARG
     	;;
	k) kaijuDb=$OPTARG
	;;
	h) printf "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1
	     ;;
	esac
done

shift $((OPTIND - 1)) #remove options leave arguments 

#startof the pipeline calling the different part of the pipe


#quality filtering calling trimmomatic default settings.

EggVio_trimmomatic_qualFilt.sh -d $rawData

cd $rawData

#call megahit using default settings for the script

EggVio_megahit_assembly.sh

cd $rawData

#now let's go for bowtie index db building for mapping

EggVio_bowtie2_indexDb.sh -f $rawData/megahit_assembly/final.contigs.fa -t $threads 

cd $rawData

#then map back reads on contigs

EggVio_bowtie2_map_to_indexDb.sh -i $rawData/megahit_assembly/Index -d $rawData/trimmomatic_filtering  -t $threads



#generate coverage files and bam for anvio

cd $rawData

EggVio_samToBam_and_coverage.sh -d  $rawData/Mapping

#Now let's go for anvio contigs db and profile db

cd $rawData

EggVio_anvio_condigDb.sh -f $rawData/megahit_assembly/final.contigs_anvio_2500.fa -n $datasetName -t $threads

#retrieve taxonomy from contigs genes using kaiju and add it to contigs dbgenerated previously

cd $rawData

EggVio_kaiju_taxonomy_to_db.sh -f $rawData/megahit_assembly/anvio_contigs.db -r $kaijuDb -t $threads 

#Now merge everything into a profile.db

cd $rawData

EggVio_anvio_profile_and_merge_bam.sh -d $rawData/bam_files -f  $rawData/megahit_assembly/anvio_contigs.db -t $threads 

cd $rawData


#Now predict genes and annotate with eggnog 
module load R/3.4.3-foss-2017b-X11-20171023

 
EggVio_prodigal_gene_prediction.sh -f $rawData/megahit_assembly/final.contigs_anvio_2500.fa -c $rawData/covResults/read_counts/read_counts_final.tab -t $threads 

#submit jobs to run in parallel
cd $rawData

EggVio_submit_reads_to_eggnogmapper.sh -r  $rawData/Mapping   -n $datasetName -l $limitNumJobs -u $userName  -p $partitionName  -d $durationTime -t $threads


while (( $( squeue -u $userName |grep 'eggmap_' |wc -l) > 0 )) #Here I check if submitted jobs are finished since I need to run the next stage of my pipeline only if the reads annotations are available so this is a must to check the state of the jobs in the queue.
do
  sleep 15m	
done



#now merge annotations from reads and contigs in one single table
cd $rawData

EggVio_merging_reads_contigs_annotations.sh -f "$rawData/megahit_assembly/Genes_Prediction_and_annotation/final_eggnog_contigs_abundance_table.tab" -c "$rawData/megahit_assembly/Genes_Prediction_and_annotation/"  -r "$rawData/auto_submit_EggVio/$datasetName/" -t $threads

module unload R/3.4.3-foss-2017b-X11-20171023
