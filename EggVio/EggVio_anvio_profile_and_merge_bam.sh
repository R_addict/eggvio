#!/bin/bash
#goal: anvio bam processing against database of contigs



directoryBam="./bam_files"  fileContigDb=' ./megahit_assembly/anvio_contig.db' lengthThreshold=2500 thread=1
usage="\nUsage: $0 [-d directoryBam] [-f fileContigDb] [-l lengthThreshold] [-t thread] \n
-d\tpath to the directory containing all the .bam files to be integrated in anvio profile for coverage information. (Default: ./bam_files)
-l\tThe threshold length (in bp) to keep contigs in the profile db for binning. (Default: 2500)
-f\tthe path to the contig database file to merge all the bam file with. (Default: ./megahit_assembly/anvio_contig.db)
-t\tthe number of threads to use with anvio to compute the coverage per sample. (Default:1) \n"


while getopts :d:f:l:t:h opt  
do
    case $opt in
        d) directoryBam=$OPTARG
        ;;
        f) fileContigDb=$OPTARG
	;;
	l) lengthThreshold=$OPTARG
	;;
	t) thread=$OPTARG
        ;;
        h) printf  "$usage" >&2
           exit 0
        ;;
        '?') printf "$0: invalid option -$OPTARG" >&2
             printf "$usage" >&2
             exit 1
             ;;
        esac
done

shift $((OPTIND - 1)) #remove options leave arguments       


##load anvio env
source activate anvio_env
cd $directoryBam
outputDir=../anvio_profiles
mkdir $outputDir

for file in ./*.bam
do
fileName=$(basename $file)
sampleName=${fileName%.bam}
sampleIdRaw=${sampleName%%_*}
#before to give the names of the samples, I need to remove the union marks since anvio doesn't like them but they were good for me to avoid being confused by other underscores in the whole file names and keep samplename together.
sampleId=$( sed 's/-/_/g' <<<$sampleIdRaw )
#learning all the time, here the sample names can't start with digits so I'll just add a small check and if itstart put sample in front^^
if [[ $sampleId==[0-9]* ]]
then
sampleId=Sample_$sampleId
fi

anvi-init-bam $file -o ${sampleName}_anvi.bam
anvi-profile -i ${sampleName}_anvi.bam -c $fileContigDb -T $thread --output-dir  $outputDir/$sampleId/ --sample-name $sampleId --min-contig-length $lengthThreshold
done
anvi-merge $outputDir/*/PROFILE.db -o ../SAMPLES-MERGED -c $fileContigDb
#deactivate anvio env

conda deactivate
