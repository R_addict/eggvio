# !/bin/bash/
##author: benoit Bergk Pinto
#use: remove adapter, quality filter and trim paired end reads

#load module java
module load Java/1.8.0_77

usage="\nUsage: $0 [-d directoryFastqgz] [-e extensionFile]  [-m minlen]   [-a adapterSet] \n
-d\tpath to the directory containing the fastq files to process. (Default: current directory)
-e\textension of the fastq files to filter.(Default: fastq.gz) 
-m\tthe minimum length (in bp) of reads after quality filtering and trimming that trimmomatic needs to keep. (Default: 36)
-a\tthe name of the adapters to remove during quality filtering. check names in the folder called adapters located in the folder of trimmomatic located at $HOME/softwares/Trimmomatic-0.38/adapters/ (Default: NexteraPE-PE.fa:2:30:10) \n"
directoryFastqgz="."  extensionFile='fastq.gz'  minlen=36 adapterSet='NexteraPE-PE.fa:2:30:10'

while getopts :d:e:m:a:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that \
#it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all argumen#t\
#s is to avoid getopts to throw any automatic errors!!!!                                     
do
    case $opt in
	d) directoryFastqgz=$OPTARG
	;;
        e) extensionFile=$OPTARG
	;;
	m) minlen=$OPTARG
	;;
	a) adapterSet=$OPTARG
        ;;
        h) printf  "$usage" >&2
           exit 0
        ;;
        '?') echo "$0: invalid option -$OPTARG" >&2
             echo "$usage" >&2
             exit 1
             ;;
        esac
done

shift $((OPTIND - 1)) #remove options leave arguments                                                                        

#set nullglob option to avoid bash to search for pattern literrally if empty search
shopt -s nullglob

          
cd $directoryFastqgz

trimDir=trimmomatic_filtering

mkdir $trimDir
mkdir $trimDir/paired_reads/
mkdir $trimDir/unpaired_reads/

for file in $directoryFastqgz/*_R1_*.${extensionFile}
do
echo $file
#generate a sample name
samplefastqgz=$(basename $file)
samplename=${samplefastqgz%%_R1*}
endOfFile=${samplefastqgz#*_R1}

java -jar $HOME/softwares/Trimmomatic-0.38/trimmomatic-0.38.jar PE -phred33 ${samplename}_R1${endOfFile} ${samplename}_R2${endOfFile} $trimDir/paired_reads/${samplename}_R1_001_paired.fq.gz $trimDir/unpaired_reads/${samplename}_R1_001_unpaired.fq.gz $trimDir/paired_reads/${samplename}_R2_001_paired.fq.gz $trimDir/unpaired_reads/${samplename}_R2_001_unpaired.fq.gz ILLUMINACLIP:$HOME/softwares/Trimmomatic-0.38/adapters/$adapterSet LEADING:3 TRAILING:3 SLIDINGWINDOW:4:15 MINLEN:$minlen
done
module unload Java/1.8.0_77 
