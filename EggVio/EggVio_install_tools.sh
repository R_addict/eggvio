#!/bin/bash
#date 26th of March 2019
#author B. Bergk Pinto
#aim: install tools required to use the script

#install anaconda
curl -O https://repo.continuum.io/archive/Anaconda3-5.0.1-Linux-x86_64.sh
#check anaconda package integrity
sha256sum Anaconda3-5.0.1-Linux-x86_64.sh
#install anaconda
bash Anaconda3-5.0.1-Linux-x86_64.sh

#reload .bashrc to get the changes made by anaconda
source ~/.bashrc


#to do in case of old install, update your conda installer

conda update conda


#create a directory called softwares 
mkdir $HOME/softwares
cd $HOME/softwares

#in this you'll will install trimmomatic and also eggnogmapper

#eggnog mapper download 

git clone https://github.com/jhcepas/eggnog-mapper.git

#trimmomati download

wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.38.zip

unzip Trimmomatic-0.38.zip

#download ARG_OAP soft from git

git clone  https://github.com/biofuture/Ublastx_stageone.git

#create an environment for bedtools, samtools, bowtie and prodigal

conda create -n mapping_env python=3.6 anaconda
#activate environment

source activate mapping_env

#install the tools

conda install -c bioconda bedtools

conda install -c bioconda bowtie2

conda install -c bioconda prodigal

conda install -c bioconda --freeze-installed samtools

#close environment

conda deactivate 

#install anvio in a new environment

conda create -n anvio_env -c conda-forge -c bioconda anvio==5.4.0 "blas=*=openblas"

#activate envi

source activate anvio_env

#install anvio and kaiju for taxonomy annotation

conda install -c bioconda kaiju 

conda install -c anaconda wget

#test anvio

anvi-self-test --suite mini

#install cog db for anvio

anvi-setup-ncbi-cogs --just-do-it 

#close env

conda deactivate

