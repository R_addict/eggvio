#!/bin/bash
#date 15 January 2019
#aim: compute coverage of contig for each sample startring from sam files from mapping with bowtie
#argument: just the directory where the sam files are stored





directoryBam="." 
usage="\nUsage: $0 [-d directorySam]\n
-d\tthe path to the directory where the .sam files to convert into bam are located. (Default: current directory)
\n"


while getopts :d:h opt
do
    case $opt in
        d) directorySam=$OPTARG
        ;;
        h) printf  "$usage" >&2
           exit 0
        ;;
        '?') printf "$0: invalid option -$OPTARG" >&2
             printf "$usage" >&2
             exit 1
             ;;
        esac
done

shift $((OPTIND - 1)) #remove options leave arguments     


source activate mapping_env


cd $directorySam
mkdir ../covResults
mkdir  ../covResults/intermediate/
mkdir ../bam_files
mkdir ../covResults/read_counts/ #will store counts from reads mapped on the contigs here

for file in ./*mapping.sam
do
	name=$(basename $file)
	sampleName=${name%_mapping.sam}
	samtools sort $file >  "../bam_files/${sampleName}.bam"
	samtools index  "../bam_files/${sampleName}.bam"  	
	#idxstats generate a file with the following columns 1#contigName 2#contigSIze 3#mapped reads 4#unmaped reads (paired reads which didn't hit in the same contig as the other or didnt' mapped at all)
	bedtools genomecov   -ibam "../bam_files/${sampleName}.bam" > "../covResults/intermediate/${sampleName}.cov"
	sed -i -e '/^genome/ d' ../covResults/intermediate/${sampleName}.cov #useless lines which are sumarizing coverage for whole assembly contigs I think (each contig is treated like a chromosome from a genome)
	contigIds=$(awk '{print $1}' ../covResults/intermediate/${sampleName}.cov|sort|uniq) 
	echo 'ContigId	Coverage'>../covResults/${sampleName}_coverage.tab
	fileCov="../covResults/intermediate/${sampleName}.cov"
#generate counts coverage results only
	samtools idxstats "../bam_files/${sampleName}.bam" >"../covResults/read_counts/${sampleName}_counts.tab"
	for contig in $contigIds
	do	
		contigCov=0
		while read line 
		do   
			cov=$(echo $line|awk '{print $2}' )  #coverage value
			perc=$(echo $line|awk '{print $5}' ) #percent of the contig having such coverage
			contigCov=$(echo   "$contigCov +$perc * $cov"|bc)
		done <<< "$(awk "/^${contig}\t/" $fileCov)"  #be carefull to add still double quotes for the whole expression for the here string. this prevent the while loop to be executed in a nutshell
		echo "coverage for the contig $contig done!"
		echo "$contig	$contigCov">>../covResults/${sampleName}_coverage.tab
	done &  #here the & is making each for loop for contig coverage to run as a background process so it parallelize well if the number of cores are more or les equal to number of iterations of the loop
 done 
#remove an intermediate file for counts generation
rm "${fastaContigs}.bed"
conda deactivate


#now let's fuse the reads counts
#initialise an empty file that will contain all the coverage
cd ../covResults/read_counts/
printf 'Contigs_Ids'>read_counts_final.tab #during loop contains only the header = sample names
>workingFile #during the loop it bwill contain the columns by alternation with tmp
>tmp
iter=1
for files in ./*_counts.tab
do	
   Name=$(basename ${files%%_*}) #name of the sample
   #run some test to get rid of - and starting numbers in samples ids in order to avoid any issues with R after 
   Name=$( sed 's/-/_/g' <<<$Name )
   #learning all the time, here the sample names can't start with digits so I'll just add a small check and if itstart put sample in front^^
   if [[ $Name==[0-9]* ]]
   then
       Name=Sample_$Name
   fi

   echo $Name
   if [ $iter == 1 ]
   then
       echo 'case1'
       awk '{print $1,$3}' $files >workingFile

   else
       echo 'case2'
       awk 'NR==FNR{a[NR]=$0;next}{print a[FNR],$3}' workingFile $files >tmp
       cat tmp >workingFile
   fi
   sed -i "1s/$/\t$Name/"  read_counts_final.tab #now after first coluns, nee to a tab to make the same sepration between headers of columns (samplenames) than the columns itsel
   iter=$(( $iter + 1 ))
done



#just merge headers with columns values in the final file
printf "\n" >>read_counts_final.tab 
cat workingFile >>read_counts_final.tab
sed -i '$ d' read_counts_final.tab 
#in fact I just realized that columns were separated by spaces instead of tab so I just want to replace it
sed -i 's/ /\t/g' read_counts_final.tab 
rm tmp #remove a file used to store calculations only
rm  workingFile
