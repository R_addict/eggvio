#!/bin/bash
#author: Benoit Bergk pinto
#Date: 19-11-2018
#--------------------------------------
#Objective: merge contigs annotations with reads annotations
#variables


readsAnnotFolder= contigsAnnotFolder=  fileOfContigsCounts=  thread=1

usage="\nUsage: $0 [-f fileOfContigsCounts] [-c  contigsAnnotFolder] [-r readsAnnotFolder] [-t thread] \n
-f\tpath to the file containing the counts for each of the contigs annotations. Mandatory parameter.                      
-c\tpath to the folder containing the annotation files of the contigs. Mandatory parameter
-r\tpath to the folder containing the annotations of the reads from teh different samples. Mandatory parameter
-t\tthe number of threads to use with the R script to merge all the annotations. (Default:1) \n"

while getopts :r:c:f:t:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	r) readsAnnotFolder=$OPTARG
	;;
	c) contigsAnnotFolder=$OPTARG
	;;
	f) fileOfContigsCounts=$OPTARG
	;;
	t) thread=$OPTARG
     	;;
	h) printf "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1
	     ;;
	esac
done

shift $((OPTIND - 1)) #remove options leave arguments

pathToEggVio=${0%/*} #remove name of the current script
cd ${readsAnnotFolder%/*}
cd ../../
mkdir final_annotation_Tables
cd ./final_annotation_Tables

Rscript $pathToEggVio/eggNog_table_make.R  $readsAnnotFolder $contigsAnnotFolder $fileOfContigsCounts
