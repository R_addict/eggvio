#!/bin/bash
#author: Benoit Bergk Pinto
#date: 18th September 2017
#aim: use eggnoggmaper to annotate metagenomes and metatranscriptomes

#link to eggnoggmapper command line help page: https://github.com/jhcepas/eggnog-mapper/wiki/Command-Line-Options
##Note: this script works with a specific submission script that modifies the input variables in order to generate the right script to submit

##submission variables for sbatch

#SBATCH --job-name=toname
#SBATCH --output=job.%j.out
#SBATCH --error=job.%j.err
#SBATCH --mail-type=ALL
#
#SBATCH --partition=partitionName
#SBATCH --nodes=1
##SBATCH --cpus-per-task=numberThreads
#SBATCH --mem=32000 # amount of RAM memory required per node, in Mega Bytes
#SBATCH --time=hoursOfRun:00:00




##input variables
extension=specify   
inputDir=inputfolder
sampleName=toMod #samplename is the name of the sample without the extension so can be used to specify the output name also ;)
output_dir=outputfolder



mkdir  /tmp/bbergkpi/
cd /tmp/bbergkpi/
cp $inputDir/${sampleName}${extension} ./
#cp $HOME/softwares/eggnog-mapper/ $run_dircd $run_dir


## Command line for eggnog mapper

python2.7 $HOME/softwares/eggnog-mapper/emapper.py -i "$sampleName$extension" -o $sampleName --keep_mapping_files -m diamond --translate --cpu numberThreads  --usemem --seed_ortholog_evalue '0.001' --seed_ortholog_score '60' --output_dir "$output_dir" 


#personnal note: no target database required when using diamond tool

##print a message for end of script

echo "Mapping and annotation of the sample $sampleName done with success!"


