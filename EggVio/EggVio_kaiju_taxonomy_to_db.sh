#!/bin/bash
#author: Benoit Bergk pinto
#Date: 16-04-2019
#--------------------------------------
#Objective: add kaiju taxonomy to the contigs of the contig db in anvio
#variables


fileContigDb= referenceKaijuDbFolder=  thread=1

usage="\nUsage: $0 [-f fileContigDb] [-r referenceKaijuDbFolder] [-t thread] \n
-f\tpath to the contig.db of anvio in which the contigs need to be taxonomically annotated using kaiju. Mandatory parameter.
-r\tpath to the folder containing kaiju database and nodes.tmp files. Mandatory Parameter.
-t\tthe number of threads to use with kaiju to annotate contigs with their taxonomy. (Default:1) \n"

while getopts :f:r:t:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	f) fileContigDb=$OPTARG
	;;
	r) referenceKaijuDbFolder=$OPTARG
	;;
	t) thread=$OPTARG
     	;;
	h) printf  "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1
	     ;;
	esac
done

shift $((OPTIND - 1)) #remove options leave arguments

#shopt -s nullglob

source activate anvio_env

workDir=${fileContigDb%/*} #extract path of contig.db to work there
cd $workDir

anvi-get-sequences-for-gene-calls -c $fileContigDb -o gene_calls.fa

#run kaiju
kaiju -t ${referenceKaijuDbFolder}/nodes.dmp -f ${referenceKaijuDbFolder}/kaiju_db*.fmi -i gene_calls.fa -o gene_calls_nr.out -z $thread -v
        
#add taxonomy levels 
addTaxonNames -t ${referenceKaijuDbFolder}/nodes.dmp -n ${referenceKaijuDbFolder}/names.dmp -i gene_calls_nr.out -o gene_calls_nr.names -r superkingdom,phylum,order,class,family,genus,species
  
#import this result into anvio contig db
anvi-import-taxonomy-for-genes -i gene_calls_nr.names -c $fileContigDb  -p kaiju --just-do-it #anvio is throwing errors if not to be sure we don't ruin a db by choosing wrong stuff before

conda deactivate
