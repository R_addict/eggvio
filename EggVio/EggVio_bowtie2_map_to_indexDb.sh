#!/bin/bash
#author: Benoit Bergk pinto
#Date: 19-11-2018
#--------------------------------------
#Objective: map reads from a sample back to the contig ref db

#pre-requisite: have un zipped fastq for mapping in a result directory 

#Variables used
#$1 = path to contig db NAME (wo extension)
#$2 = path to all the fastq.gz files trimmomatic directories
#$3 = path to result directory to create
#$4 = amount of threads

indexDbPAth='Index' directoryFastFilt= outputDirectory='./Mapping' thread=1

usage="\nUsage: $0 [-i indexDbPAth] [-d directoryFastFilt] [-o outputDirectory] [-t thread] \n
-i\tthe path to the index database to use for mapping back the reads. The name of the db must be specified without the extension.(Default:)
-d\tpath to the directory containing the trimmomatic filtered fastq. Mandatory argument
-o\tname of the new directory to create to store the results of the mapping. (Default: Mapping)
-t\tthe number of threads to use with bowtie2 to compute the index. (Default:1) \n"

while getopts :i:d:o:t:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	i) indexDbPAth=$OPTARG
	;;
	d) directoryFastFilt=$OPTARG
	;;
	o) outputDirectory=$OPTARG
	;;
	t) thread=$OPTARG
     	;;
	h) printf "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1

	;;
	esac
done

shift $((OPTIND - 1)) #remove options leave arguments

#set nullglob option to avoid bash to search for pattern literrally if empty search
shopt -s nullglob




mkdir $outputDirectory
for file in $directoryFastFilt/*/*.fq.gz #Here I just ask to take into account all the possible fastq extensio$
do
cp $file $outputDirectory
done

cd $outputDirectory
gunzip *.fq.gz
for filecp in ./*_R1_*_paired.fq
do
samplefastq=$(basename $filecp)
samplename=${samplefastq%%_R1_*.fq} 
endOfFile=${samplefastq#*_R1_}
enOfFileUnpair=$(sed 's/paired/unpaired/g' <<<$endOfFile)
cat "${samplename}_R1_${enOfFileUnpair}" "${samplename}_R2_${enOfFileUnpair}"   > "${samplename}_unpaired.fq"

#very sensitive sice -L is 4 but except that default parameters so 15 % mismatches tolerated and oly best align is returned
source activate mapping_env

echo $(wc -l "${samplename}_R1_${endOfFile}")
echo $(wc -l "${samplename}_R2_${endOfFile}")

bowtie2 --threads $thread -q --fr -N 0 -L 4 -x $indexDbPAth  -1 "${samplename}_R1_${endOfFile}" -2 "${samplename}_R2_${endOfFile}" -U  "${samplename}_unpaired.fq" --al "${samplename}_al_unpaired_reads_ids"  --al-conc "${samplename}_aligned_reads_ids" --un "${samplename}_unal_unpaired_reads_ids"  --un-conc "${samplename}_unaligned_reads_ids" -S "${samplename}_mapping.sam"
done 



rm *.fq
rm *.fq.gz

conda deactivate
