#!/bin/bash
#author: Benoit Bergk pinto
#Date: 23-03-2019
#--------------------------------------
#Objective: Detect ARGs in metagenomic reads

#pre-requisite: have un zipped fastq for mapping in a result directory

#Variables used
#$1 = path to all the fastq.gz files trimmomatic directories
#$2 = path to result directory to create


directoryFastqFiltPaired= outputDirectory='ARG_OAP_results' lengthPrefixCategory='1'

usage="\nUsage: $0 [-d directoryFastqFiltPaired] [-o outputDirectory] [-l lengthPrefixCategory]\n
-d\tpath to the directory containing the filtered paired .fq.gz files. Mandatory argument.
-o\tpath and name of the output directory where the results of the search will be stored.
-l\tlength of the prefix in the names of the fastq.gz files that define the group of the sample for comparison with the tool.\n"

while getopts :d:o:u:l:t:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	d) directoryFastqFiltPaired=$OPTARG
	;;
	o) outputDirectory=$OPTARG
	;;
	l) lengthPrefixCategory=$OPTARG
	;;
	h) printf "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1
	     ;;
	esac
done

shift $((OPTIND - 1)) #remove options leave arguments

#set nullglob option to avoid bash to search for pattern literrally if empty search
shopt -s nullglob

#Here, I move my files since I need to rename them and I'll also create a metadata file after
mkdir $outputDirectory
cd $outputDirectory
sampleID=1
printf "SampleID\tName\tCategory\n"> metadata.tsv

for file in ${directoryFastqFiltPaired}/*.fq.gz #Here I just ask to take paired reads samples
do
cp $file .
#need to create a new name for all the different 
fileName=$(basename $file)
sampleName=${fileName%%_*}

readSide=$(sed -e 's/.*_R//g' <<< $file)
readSide=${readSide%%_*} ## so 1 or 2 if the read is reverse or forward fq
#change name of the fastq file
mv "$fileName" "${sampleName}_${readSide}.fq.gz"
#extract first letter of sample name to get a sampleType
#this step is specific to micrococsm samples where control samples start with C and acetate amended samples start with A
sampleType=${fileName:0:$lengthPrefixCategory}
#now let's save new name in metadata file BUT only if R1 to get only one entry per sample
if [ $readSide == 1  ]
then
    printf "${sampleID}\t${sampleName}\t${sampleType}\n" >>metadata.tsv
    sampleID=$(( $sampleID + 1))
fi
done

#the files are ready for mining with tool now
module load R/3.4.3-foss-2017b-X11-20171023

$HOME/softwares/ARGs_OAP/Ublastx_stageone/argoap_pipeline_stageone_version2 -i . -o ARG_output -m metadata.tsv -s -z
#need to load R for second step of the pipe


$HOME/softwares/ARGs_OAP/Ublastx_stageone/argoap_pipeline_stagetwo_version2 -i ./ARG_output/extracted.fa  -m ./ARG_output/meta_data_online.txt -o ./ARG_output/final_results 

#close R module
module unload R/3.4.3-foss-2017b-X11-20171023





