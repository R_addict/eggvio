#!/bin/bash
#author: Benoit Bergk pinto
#Date: 27-03-2019
#--------------------------------------
#Objective: do some gene prediction based on assembled contigs, annotate them using eggnog mapper and then merge annotate with read counts from coverage
#variables

fastaOfContigs=  thread=1 coverageFile="./covResults/read_counts/read_counts_final.tab"

usage="\nUsage: $0 [-f fastaOfContigs ] [-c coverageFile] [-t thread] \n
-f\tpath to the fasta of the contigs selected to import in anvio. Mandatory parameter.
-c\tpath to the coverage file sumarizing abundances in counts. (Default: ./covResults/read_counts/read_counts_final.tab)                      
-t\tthe number of threads to use with prodigal and eggnogmapper to compute the index. (Default:1) \n"

while getopts :f:c:t:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all arguments is to avoid getopts to throw any automatic errors!!!!
do
	case $opt in
	f) fastaOfContigs=$OPTARG
	;;
	t) thread=$OPTARG
     	;;
	c) coverageFile=$OPTARG
	;;
	h) printf "$usage" >&2
           exit 1
        ;;
	'?') printf "$0: invalid option -$OPTARG" >&2
	     printf "$usage" >&2
	     exit 1
	     ;;
	esac
done

shift $((OPTIND - 1)) #remove options leave arguments

#get name of fasta file
fastaOfContigsName=$(basename $fastaOfContigs)
fileName=${fastaOfContigsName%.fa}


source activate mapping_env
#Create a result directory to write the results inside
resDir="${fastaOfContigs%/*}/Genes_Prediction_and_annotation"
mkdir $resDir
cd $resDir
prodigal -i $fastaOfContigs -o ${fileName}_prodigal_coords.gbk -a ${fileName}_prodigal_proteins.faa -d ${fileName}_prodigal_nucleotide.fa -p meta

conda deactivate


#annotation using emapper

python2.7 $HOME/softwares/eggnog-mapper/emapper.py -i ${fileName}_prodigal_nucleotide.fa -o "$fileName" --keep_mapping_files -m diamond --translate --cpu "$thread" --usemem --seed_ortholog_evalue '0.001' --seed_ortholog_score '60' --output_dir $resDir


#generate contigs names from the fasta files from prodigal

grep -E '^>' ${fileName}_prodigal_nucleotide.fa >geneNames.txt
#just remove all comments to get clean names
sed -i -E 's/#.*//g' geneNames.txt
sed -i -E 's/^>//g' geneNames.txt

#now, we need to transform the header of the coverageFile before to give it to R since R doesn't like, as anvio - and 

#merge annotation with counts generated from the previous coverage thing
pathToEggVio=${0%/*} #remove name of the current script

Rscript $pathToEggVio/merge_annot_contigs.R  "${fileName}.emapper.annotations" geneNames.txt $coverageFile

rm geneNames.txt




#anvi-get-sequences-for-gene-calls -c contigs.db -o gene_calls.fa --report-extended-deflines
#An alternative if want to extract from anvio's predictions but other genes like 16S prdictions are also in ther so more confused
