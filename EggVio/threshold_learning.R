##################################################
## Project: EggVio
## Script purpose: define a threshold for read annotation based on an input file of results coparisons
## Date: 2nd November 2019
## Author:B. Bergk Pinto
##################################################
## Libraries used in this script
##################################################

## Section: Input user's argument
##################################################
  
args = commandArgs(trailingOnly=TRUE)

#for debug
#args = c('~/ownCloud/EggVio_publi/MISS/threshold_learner_file_ko.tab', 0.05)

if (length(args)<2) {
  stop("The script requires the file threshold_learner_file.tab, a False positive threshold, and a save name for the result table", call.=FALSE)
} else if (length(args)==2) {
  # default output file
  print("Threshold learning started")
}

## Section: import file
##################################################

learningData <- read.table(file = args[1], header = FALSE,sep = ' ',stringsAsFactors = FALSE)  

## Section: remove unusable contigs that don't have any ko info so column 4 =MissingAnnot and same in the other so can't say if it's right or wrong
##################################################
#learningData <- learningData[-which(learningData$V4=='MissingAnnot' & learningData$V5=='MissingAnnot'),]


## Section: make two distinct data sets one for good match = 1 and bad match = 0
##################################################
goodMatch <- learningData[which(learningData$V1==1),]  

badMatch <- learningData[which(learningData$V1==0),]  

## Section: Find the threshold based on quantile so get less than an certain amout of bad
##################################################
  
pValThresh <-  as.numeric(args[2])
#Basicall this quantile threshold is the amount of mistake we want to remove from our annotation so we will select the value above it as threshold from the data we observed here

#idea came from the post of Macro from stack overflow to just use the data and no interpolation from it! More robust


#here I create a function computing amount of true positive to see when I reach a ratio where more than threshol p-val are correct
truePosFind = function(dataF,stepN, modeU,startPos){
  if(modeU == 'raw'){
    roundNumb <- nrow(dataF) - (nrow(dataF) %% stepN) 
    stepMax <- roundNumb / stepN
  }else{
    stepMax <- 10
  }
  res <- data.frame(rowN=rep(NA,stepMax+1), Fpos= rep(NA,stepMax+1))
  for(iter in c(1:stepMax)){
    if(iter == 1){
      res$rowN[1] <- startPos
      res$Fpos[1] <- length(which(dataF[c(startPos:nrow(dataF)),1]==0))/(nrow(dataF)-startPos+1)
    }
    res$rowN[iter+1] <- (iter*stepN)+startPos
    fpos <- length(which(dataF[c(((iter*stepN)+startPos):nrow(dataF)),1]==0))/(nrow(dataF)-((iter*stepN)+startPos))
    if(length(fpos)==0){
      res$Fpos[iter+1] <- 0
    }else{
      res$Fpos[iter+1] <- fpos
    }
  }
  return(res)
}

#here I'll use the function above and iterate through a lot of steps of smaller and smaller sizes to refine the result of threshold

steps <- c(1000,100,10,1)
dataForLearn <- learningData[order(learningData$V2,decreasing = TRUE),]

for(stepSize in steps){
  if(stepSize == 1000){
    threshRes <- truePosFind(dataForLearn,stepSize,'raw',1)
  }else{
    threshRes <- truePosFind(dataForLearn,stepSize,'default',whereToCut)
  }
  print(threshRes)
   if(pValThresh < min(threshRes$Fpos)){
     errorM <- paste('The p-value threshold setted is smaller than the best result possible to reach for this dataset. Please set it above the following minimum possible:', min(threshRes$Fpos))
     stop(errorM)
   }
    whereToCut <- threshRes$rowN[which(threshRes$Fpos  < pValThresh)[1]] 
    if(stepSize > 1){
      whereToCut<-  whereToCut - stepSize
    }
    threshold_seed_ortholog_evalue <- dataForLearn[whereToCut,2]
    exactPval <- round(threshRes$Fpos[which(threshRes$Fpos  < pValThresh)[1]],5)
   
}


## Section: See how much false negative we get after choosing this threshold
##################################################
dataFilteredAfterThresh <- learningData[which(learningData$V2 <= threshold_seed_ortholog_evalue),]

#what the user will reject from his results based on the whole annotation
rejected <- nrow(learningData[which(learningData$V2 > threshold_seed_ortholog_evalue),])
  
falseNeg <- length(which( goodMatch$V2 > threshold_seed_ortholog_evalue  ))/rejected

trueNeg <- length(which( badMatch$V2 > threshold_seed_ortholog_evalue  ))/rejected
#fraction of good and bad rejected
trueAndreject <- length(which( goodMatch$V2 > threshold_seed_ortholog_evalue  ))/nrow(goodMatch)

falseANdReject <- length(which( badMatch$V2 > threshold_seed_ortholog_evalue  ))/nrow(badMatch)
# what the person will receive as expected results

falsePos <- length(which( badMatch$V2 <= threshold_seed_ortholog_evalue  ))/nrow(dataFilteredAfterThresh)

truPos <- length(which( goodMatch$V2 <= threshold_seed_ortholog_evalue ))/nrow(dataFilteredAfterThresh) 

## Section: Write this as a table to summarize every information
##################################################

resultsTable <- data.frame(Pval_set=pValThresh,Threshold_eValue=threshold_seed_ortholog_evalue, False_Negative=round(falseNeg,5),
                           True_Negative=round(trueNeg,5), False_Positive=round(falsePos,5), 
                           True_positive=round(truPos,5),Fraction_of_correct_rejected= round(trueAndreject,5), Fraction_of_false_rejected=round(falseANdReject,5) )

write.table(x = resultsTable,file = args[3],quote = FALSE,sep = '\t',row.names = FALSE)
q('no')
