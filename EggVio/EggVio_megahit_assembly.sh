#!/bin/bash
#author: Benoit Bergk pinto
#Date: 19-11-2018
#--------------------------------------
#Objective: use megahit to assemble a metagenome dataset or a single library since for the moment, spades meta isn't able to assemble more

#set path to megahit
megahitPath="/softs/manual/ampere/megahit/"
threads=1



usage="\nUsage: $0 [-d directoryFastFilt] [-c complexity] [-t threads] \n                                                                         
-d\tpath to the directory containing the trimmomatic quality filtered fastq. (Default: ./trimmomatic_filtering)                                          
-c\tcomplexity of the metagenomes to filter. For soils, which are very complex, this parameter should be set on complex(Default: regular) 
-t\tthe number of cores used by megahit for the assembly. (Default: 1)\n"

directoryFastFilt="./trimmomatic_filtering"  complexity='regular' #or complex

while getopts :d:c:h opt   #critical point all the arguments are put and followed by : if they expect an argument so p: says that \    
#it expect an argument after so for the help message we want it empty so we need to write h and NOT h: .the first : before all argumen\
#t\                                                                                                                                    
#s is to avoid getopts to throw any automatic errors!!!!                                                                               
do
    case $opt in
        d) directoryFastFilt=$OPTARG
        ;;
        c) complexity=$OPTARG
        ;;
        t) threads=$OPTARG
	;;
	h) printf  "$usage" >&2
           exit 0
        ;;
        '?') printf "$0: invalid option -$OPTARG" >&2
             printf "$usage" >&2
             exit 1
             ;;
        esac
done

shift $((OPTIND - 1)) #remove options leave arguments                                                                                 
saveDir="../megahit_assembly"

#issue if output directory already exist so just try to erase before to use the script
rm -r $directoryFastFilt/-t                                    

cd $directoryFastFilt
cat ./paired_reads/*_R1_*.fq.gz >R1.fq.gz
cat ./paired_reads/*_R2_*.fq.gz >R2.fq.gz
cat ./unpaired_reads/*.fq.gz >unpaired.fq.gz

	
if [ $complexity == 'regular' ]
then
 ${megahitPath}megahit -1 R1.fq.gz -2 R2.fq.gz -r unpaired.fq.gz -o $saveDir -t $threads --min-count 2 --k-list 21,41,61,81,99 
elif [ $complexity == 'complex' ]
then
 ${megahitPath}megahit -1 R1.fq.gz -2 R2.fq.gz -r unpaired.fq.gz -o $saveDir -t $threads --min-count 2 --k-list 27,37,47,57,67,77,87 
else
 printf 'complexity parameter not set to the any allowed value \n' >&2
 printf $usage >&2
 exit 1
fi
rm R1.fq.gz
rm R2.fq.gz
rm unpaired.fq.gz


